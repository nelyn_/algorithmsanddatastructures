package org.algorithms;


import java.util.*;

public class Search<T extends Number>
{
    static Random rd = new Random();

    public static void task1(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(100,1000);
        }

        Integer min = Integer.MAX_VALUE;
        for (int i = 0; i < 20; i++) {
            if(x[i]<min)min=x[i];
        }

        System.out.println(Arrays.toString(x));
        System.out.println(min);
    }

    public static void task2(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(0,100);
        }

        List<Integer> less30 = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            if(x[i]<30)less30.add(x[i]);
        }

        System.out.println(Arrays.toString(x));
        System.out.println(less30);
    }


    public static void task3(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(0,100);
        }
        System.out.println(Arrays.toString(x));
        for (int i = 0; i < 20; i++) {
            if(x[i]%3==0) System.out.print(x[i]+", ");;
        }
        System.out.println();
    }

    public static void task4(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(-100,100);
        }
        System.out.println(Arrays.toString(x));
        for (int i = 0; i < 10; i++) {
            if(Math.abs(x[i])>20 && Math.abs(x[i])<50) System.out.print(x[i]+", ");;
        }
        System.out.println();
    }

    public static void task5(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(0,100);
        }
        System.out.println(Arrays.toString(x));
        for (int i = 0; i < 20; i++) {
            if(x[i]%4==0) System.out.print(x[i]+", ");;
        }
        System.out.println();
    }

    public static void task6(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(-100,100);
        }
        System.out.println(Arrays.toString(x));
        for (int i = 0; i < 20; i++) {
            if(x[i]>50) System.out.print(x[i]+", ");
        }
        System.out.println();
    }

    public static void task7(){
        Integer[] x = {-1,2,-3,4,-5,6,-7,8,-9,10};
        Integer toFind = 10;
        for (int i = 0; i < x.length; i++) {
            if(x[i].equals(toFind)) {System.out.println("Элемент найден! Число сравнений : "+i); break;}
        }
    }

    public   static Integer binarySearch(Integer[] data, Integer x) {
        int index = Integer.MAX_VALUE;
        int a = 0;
        int b = data.length;

        while (a <= b) {
            int mid = a  + ((b - a) / 2);
            if (data[mid] < x) {
                a = mid + 1;
            } else if (data[mid] > x) {
                b = mid - 1;
            } else if (data[mid] == x) {
                index = mid;
                break;
            }
        }
        return index;
    }

    public static void task8(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(0,100);
        }
        Integer key = x[0];
        System.out.println("Массив: " + Arrays.toString(x));
        Arrays.sort(x);
        System.out.println("Отсортированный: " + Arrays.toString(x));
        System.out.println("Ищем индекс элемента: " + key);
        System.out.println(binarySearch(x, key));
    }

    public static void task9(){
        Integer[] x = {345, 368, 876, 945, 564, 387, 230};
        Integer key = x[0];
        System.out.println("Массив: " + Arrays.toString(x));
        Arrays.sort(x);
        System.out.println("Отсортированный: " + Arrays.toString(x));
        System.out.println("Ищем индекс элемента: " + key);
        System.out.println("Место : "+(binarySearch(x, key)+1));
    }

    public static void task10(){
        Integer[] x = new Integer[20];
        for (int i = 0; i < 20; i++) {
            x[i]=rd.nextInt(100,1000);
        }
        System.out.println(Arrays.toString(x));
        for (int i = 0; i < x.length; i+=2) {
            System.out.print(x[i]+", ");
        }
    }


    public static void main( String[] args )
    {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
    }
}
