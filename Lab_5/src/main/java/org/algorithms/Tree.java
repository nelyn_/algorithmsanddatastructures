package org.algorithms;

import java.util.LinkedList;
import java.util.List;

public class Tree {

    static class Node{
        private Node left;
        private Node right;

        public Node(Double data){
            this.data=data;
            left=null;
            right=null;
        }
        private Double data;

        public Node getLeft() { return left;}
        public Node getRight() {return right;}
        public Double getData() {
            return data;
        }

        public void setLeft(Node left) {
            this.left = left;
        }
        public void setRight(Node right) {
            this.right = right;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    private Node root;

    public Tree(){
        root = null;
    }

    public void printValues() { toPrintValues(root);
        System.out.println(); }
    private void toPrintValues(Node root)
    {
        if (root != null) {
            toPrintValues(root.left);
            System.out.print(root.data + " ");
            toPrintValues(root.right);
        }
    }

    /** Вариант 1а*/
    public Double findLeftLeave(){
        Node current = root;
        while (current.left!=null){
            current = current.left;
        }
        return current.data;
    }

    /** Вариант 1б*/
    public Integer CountElementXInTree(Double x){
        Integer counter = 0;
        counter = toCountX(counter, x, root);
        return counter;
    }
    private Integer toCountX(Integer counter, Double x, Node root)
    {
        if (root != null) {
            counter = toCountX(counter,x, root.left);
            counter = toCountX(counter,x, root.right);
            if(root.data.equals(x))
                counter+=1;
        }
        return counter;
    }

    /** Вариант 2a*/
    public Double findAvgOfTree(){
        Double[] res = {0D, 0D};
        res = toFindAvgOfTree(res, root);
        return res[0]/res[1];
    }
    private Double[] toFindAvgOfTree(Double[] counter, Node root)
    {
        if (root != null) {
            counter = toFindAvgOfTree(counter, root.left);
            counter = toFindAvgOfTree(counter, root.right);
            counter[0]+=root.data;
            counter[1]+=1;
        }
        return counter;
    }

    /** Вариант 2б*/
    public void addAvgOfTree(){
        add(findAvgOfTree());
    }

    /** Вариант 3*/
    public void deleteAllNegative(){
        toDeleteAllNegative(root);
    }
    private void toDeleteAllNegative(Node root)
    {
        if (root != null) {
            toDeleteAllNegative(root.left);
            toDeleteAllNegative(root.right);
            if(root.data < 0D)
                delete(root.data);
        }
    }


    /** Вариант 4а*/
    public Double minOrMax(boolean min)
    {
        Node current = root;
        Double res = current.data;
        if(min){
            while (current.left != null)
            {
                res = current.left.data;
                current = current.left;}
        }else{
            while (current.right != null)
            {
                res = current.right.data;
                current = current.right;
            }
        }
        return res;
    }

    /** Вариант 4б*/
    public void printLeaves() { toPrintLeaves(root);}
    private void toPrintLeaves(Node root)
    {
        if (root != null) {
            toPrintLeaves(root.left);
            toPrintLeaves(root.right);
            if(root.left == null && root.right==null)
                System.out.print(root.data + " ");
        }
    }

    /** Вариант 5a*/
    public boolean contains(Double x) { return toContains(root, x);}
    private boolean toContains(Node root, Double x)
    {
        if (root != null) {
            boolean a = toContains(root.left, x);
            boolean b = toContains(root.right,x);
            return root.data.equals(x) || a || b;
        }
        return false;

    }

    /** Вариант 5б */
    public void addIfAbsent(Double x) {
        if(!toContains(root, x))
            add(x);
    }

    /** Вариант 6a*/
    Integer findPathLen(Double data){
        Node current = root;
        Integer counter = 0;
        while (true){
            if(current.data.equals(data)){
                return counter;
            }else{
                if(current.data>data){
                    current = current.getLeft();
                    counter++;
                }else {
                    current = current.getRight();
                    counter++;
                }
                if(current==null)return -1;
            }
        }
    }

    /** Вариант 6б*/
    public Integer findThreeDepth() {
        List<Integer> branchesLength = new LinkedList<>();
        toFindTreeDepth(root,0,  branchesLength);
        return branchesLength.stream().max(Integer::compare).orElse(-1);
    }
    private void toFindTreeDepth(Node root, Integer len, List<Integer> maxLen)
    {
        if (root != null) {
            toFindTreeDepth(root.left,len+1,  maxLen);
            if(root.left == null && root.right==null) {
                maxLen.add(len);}
            toFindTreeDepth(root.right,len+1, maxLen);
        }
    }

    /** Вариант 7 */
    public Tree copy() {
        Tree bt = new Tree();
        bt.root = toCopy(root);
        return bt;
    }

    private Node toCopy(Node focusNode) {
        if (focusNode == null) {
            return null;
        }
        Node copy = new Node(focusNode.data);
        copy.left= toCopy(focusNode.left);
        copy.right = toCopy(focusNode.right);
        return copy;
    }

    /** Вариант 8*/
    public boolean equals(Tree t) { return toEquals(root, t.root);}
    private boolean toEquals(Node root, Node troot)
    {
        if (root != null && troot!=null) {
            boolean a = toEquals(root.left, troot.left);
            boolean b = toEquals(root.right,troot.right);
            return root.data.equals(troot.data) && a && b;
        }else return root == null && troot == null;
    }


    public void add(Double data){
        if(root==null){
            root = new Node(data);
        }
        else{
            Node current = root;
            Node previous;
            while (true){
                if(current.getData() > data){
                    previous = current;
                    current = current.getLeft();
                    if(current==null){
                        previous.setLeft(new Node(data));
                        break;
                    }
                }else {
                    previous = current;
                    current = current.getRight();
                    if(current==null){
                        previous.setRight(new Node(data));
                        break;
                    }
                }
            }
        }
    }

    Node minimum(Node x){
        if(x.left == null){
            return x;}
     return minimum(x.left);
    }

    public void delete(Double z){
        root = toDelete(root, z);
    }
    private Node toDelete(Node root, Double z) {            // корень поддерева, удаляемый ключ
        if(root == null){
                return root;
        }
        if (z<root.data){
            root.left = toDelete(root.left, z);
        }else if (z > root.data){
                root.right = toDelete(root.right, z);}
          else if(root.left != null && root.right != null){
                root.data = minimum(root.right).data;
                root.right = toDelete(root.right, root.data);
          }
          else if (root.left != null)
                root = root.left;
            else if (root.right != null)
                root = root.right;
            else
                root = null;
                return root;
    }

    public static void main(String[] args) {
        Tree t = new Tree();
        Double[] testValues = {-5D, 1D, 6D, -2D, 7D, 3D, 2D, -4D, 9D, 10D, 2D, 2D};
        for (Double x: testValues) {
            t.add(x);}
        System.out.println("Значения: ");
        t.printValues();
        Tree t2 = t.copy();
        System.out.println("Эквивалентность с копией: "+ t.equals(t2));
        t2.add(100D);
        System.out.println("Эквивалентность с измененной копией: "+ t.equals(t2));


        System.out.println("Длина пути до 6: "+t.findPathLen(6D));
        System.out.println("Максимальная глубина: "+t.findThreeDepth());
        t.printValues();
        System.out.println("Элемент 3 содержится в дереве: " + t.contains(3D));
        System.out.println("Элемент 11 содержится в дереве: " + t.contains(11D));
        t.addIfAbsent(11D);
        System.out.println("Значения после добавления отсутствующего: ");
        t.printValues();

        t.printValues();
        t.deleteAllNegative();
        System.out.println("После удаления отрицательных: ");
        t.printValues();

        t.printValues();
        System.out.println();

        System.out.println("Минимум : " + t.minOrMax(true));
        System.out.println("Максимум : " + t.minOrMax(false));
        System.out.print("Значения в листьях: ");
        t.printLeaves();

        System.out.println(t.findPathLen(3D));
        System.out.println(t.findThreeDepth());

        System.out.println("Самый левый лист: "+t.findLeftLeave());
        System.out.println("Количество вхождений элемента в дерево: "+t.CountElementXInTree(2D));

        System.out.println("Значения в дереве : ");
        t.printValues();
        Double avg = t.findAvgOfTree();
        System.out.println("Среднее значение :"+ avg);
        t.addAvgOfTree();
        System.out.println("Значения в дереве после добавления среднего: ");
        t.printValues();

        System.out.println();
        t.delete( 2D);
        t.printValues();
        System.out.println();
        t.deleteAllNegative();
        t.printValues();

        System.out.println(t.contains(6D));

        Tree t3 = t.copy();
        t3.printValues();

        System.out.println(t.equals(t3));

        t3.add(100D);
        System.out.println(t.equals(t3));
    }




}

