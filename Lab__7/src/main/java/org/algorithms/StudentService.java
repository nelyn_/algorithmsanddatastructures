package org.algorithms;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class StudentService {

    static void selectionSort(List<Student> students, Comparator<Student> by)
    {
        int min;
        for (int i = 0; i < students.size() - 1; i++)
        {
            min = i;
            for (int j = i + 1; j < students.size(); j++)
            {
                if (by.compare(students.get(j),students.get(min))<0)
                    min = j;
            }
            Collections.swap(students, i, min);
        }
    }


    public static void main(String[] args) {

        String template = "     Фамилия     Имя        Год       Оценки:  англ.  мат.  физ.  прогр.  Общий\n__________________________________________________________________________________";
        Student student1 = new Student("Кошкин", "Евгений", 1990, 98,65,52,89);
        Student student2 = new Student("Мышкин", "Степан", 1955, 81,76,69,70);
        Student student3 = new Student("Арбузов", "Аркадий", 1800, 54,52,37,67);
        Student student4 = new Student("Ласточкин", "Фёдор", 1933, 73,92,57,93);



        List<Student> students = new LinkedList<>();
        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);


        selectionSort(students, Student.bySurname);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.bySurname.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byWasBorn.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byWasBorn);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byTotalScore);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byTotalScore.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byEnglishScore);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byMathScore.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byPhysicsScore);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byProgrammingScore.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byName);
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();

        selectionSort(students, Student.byName.reversed());
        System.out.println(template);
        students.forEach(System.out::println);
        System.out.println();



    }
}
