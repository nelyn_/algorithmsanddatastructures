package org.algorithms;

import lombok.Data;

import java.util.Comparator;

@Data
public class Student {

    private String surname;
    private String name;
    private Integer wasBorn;
    private Integer mathScore;
    private Integer englishScore;
    private Integer physicsScore;
    private Integer programmingScore;
    private Integer totalScore;

    public Student(String surname, String name, Integer year, Integer mathScore, Integer englishScore, Integer physicsScore, Integer programmingScore) {
        this.surname = surname;
        this.name = name;
        this.wasBorn = year;
        this.mathScore = mathScore;
        this.englishScore = englishScore;
        this.physicsScore = physicsScore;
        this.programmingScore = programmingScore;
        this.totalScore = mathScore+englishScore+physicsScore+programmingScore;
    }


    @Override
    public String toString() {
        return String.format("%10s %10s %10d      Score: %5d %5d %5d %5d    Total: %3d", this.surname, this.name, this.wasBorn,  this.englishScore,this.mathScore, this.physicsScore, this.programmingScore, this.totalScore);
    }

    public static Comparator<Student> byName= Comparator.comparing(Student::getName);
    public static Comparator<Student> bySurname = Comparator.comparing(Student::getSurname);
    public static Comparator<Student> byWasBorn = Comparator.comparing(Student::getWasBorn);
    public static Comparator<Student> byMathScore = Comparator.comparing(Student::getMathScore);
    public static Comparator<Student> byEnglishScore = Comparator.comparing(Student::getEnglishScore);
    public static Comparator<Student> byPhysicsScore = Comparator.comparing(Student::getPhysicsScore);
    public static Comparator<Student> byProgrammingScore = Comparator.comparing(Student::getProgrammingScore);
    public static Comparator<Student> byTotalScore = Comparator.comparing(Student::getTotalScore);

}