package org.algorithms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.*;

@NoArgsConstructor
public class TreeSort<T> {

    @NoArgsConstructor
    @Data
    static class   TreeNode<T> {
        TreeNode(T data){
            this.data = data;
        }
        public T data;
        public TreeNode<T> left;
        public TreeNode<T> right;
    }

    @Data
    @AllArgsConstructor
    static class BinarySearchTree<T>{
        public TreeNode<T> root;

        public void add(T x, Comparator<T> by) {
            root = recAdd(root,x,by);
        }

        private TreeNode<T> recAdd(TreeNode<T> node, T x, Comparator<T> by) {
            if (node == null) {
                return new TreeNode<T>(x);
            }
            if (by.compare(x , node.data)<0) {
                node.left = recAdd(node.left, x, by);
            } else if (by.compare(x,node.data)>0) {
                node.right = recAdd(node.right, x, by);
            }
            return node;
        }

        void sortAsc(TreeNode<T> root, List<T> res, Comparator<T> by) {
          if (root!=null) {
              sortAsc(root.left, res, by);
              res.add(root.data);
              sortAsc(root.right, res, by);
          }
        }

        @SafeVarargs
        public static <T>  BinarySearchTree<T> getBinarySearchTree(Comparator<T> by, T... args){
            if (args.length==0)return null;
            BinarySearchTree<T> tree = new BinarySearchTree<T>(new TreeNode<>(args[0]));
            for (int i = 1; i < args.length; i++) {
                tree.add(args[i], by);
            }
            return tree;
        }

    }

    @SafeVarargs
    public final List<T> sort(Comparator<T> by, T... x){
        BinarySearchTree<T> binarySearchTree = BinarySearchTree.getBinarySearchTree(by, x);
        List<T> res = new ArrayList<>();
        assert binarySearchTree != null;
        binarySearchTree.sortAsc(binarySearchTree.getRoot(), res, by);
        return res;
    }


    public static void task1(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {45, 56, 13, 75, 14, 18, 43, 11, 52, 12, 10, 36, 47, 9};
        data = Arrays.stream(data).filter(x -> x % 2 == 0).toArray(Integer[]::new);
        System.out.println(treeSort.sort(Integer::compare,data));
    }

    public static void task2(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {435,788,444,565,127,458,322,411,531,400,546,410};
        data = Arrays.stream(data).filter(x -> x / 100 == 4).toArray(Integer[]::new);
        System.out.println(treeSort.sort(Integer::compare,data));
    }

    public static void task3(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {124512,342351,765891,453122,431350,876432,734626,238651,455734,234987};
        data = Arrays.stream(data).filter(x -> ((x/100000)+((x/10000)%10)+((x/1000)%10)+((x/100)%10)+(x/10)%10 +x%10)%6 == 0).toArray(Integer[]::new);
        System.out.println(treeSort.sort(Integer::compare,data));
    }

    public static void task4(){
        TreeSort<Person> treeSort = new TreeSort<>();
        Person[] data = new Person[10];
        for (int i = 0; i < 10; i++) {
            data[i] = new Person(new Random().nextInt(18,55));
        }
        Comparator<Person> by = Comparator.comparing(Person::getAge);
        System.out.println(treeSort.sort(by.reversed(), data));
    }

    public static void task5(){
        TreeSort<Student> treeSort = new TreeSort<>();
        Student[] data = new Student[10];
        Random rd = new Random();
        for (int i = 0; i < 10; i++) {
            data[i] = new Student(
                    String.valueOf(i),
                    String.valueOf(i),
                    i,
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100)
                    );
        }
        treeSort.sort(Student.byTotalScore, data).forEach(x->System.out.print(x+", "));
        System.out.println();
    }

    public static void task6(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {11,32,23,12,6,52,47,63,69,50,43,28,35,33,42,56,55,101};
        data = Arrays.stream(data).filter(x->x%2==1).toArray(Integer[]::new);
        Comparator<Integer> by = Comparator.comparing(x->x);
        System.out.println(treeSort.sort(by.reversed(),data));
    }

    public static void task7(){
        TreeSort<EmployeeWithSalary> treeSort = new TreeSort<>();
        Random rd = new Random();
        EmployeeWithSalary[] data = new EmployeeWithSalary[10];
        for (int i = 0; i < 10; i++) {
            data[i] = new EmployeeWithSalary("Сидоров"+ i, rd.nextDouble(450,20000));
        }
        Comparator<EmployeeWithSalary> by = Comparator.comparing(EmployeeWithSalary::getSalary);
        System.out.println(treeSort.sort(by, data));
    }

    public static void task8(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {1212, 3451, 7694, 4512, 4352, 8732, 7326, 2350, 4536, 2387, 5746, 6776, 4316, 1324};
        data = Arrays.stream(data).filter(x->(((x/1000)%10)+((x/100)%10))==((x/10)%10 +x%10)).toArray(Integer[]::new);
        Comparator<Integer> by = Comparator.comparing(x->x);
        System.out.println(treeSort.sort(by.reversed(),data));
    }

    public static void task9(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Random rd = new Random();
        Integer[] data = new Integer[10];
        for (int i = 0; i < 10; i++) {
            data[i] = rd.nextInt(1000,9999);
        }
        data = Arrays.stream(data).filter(x->((((x/1000)%10)+((x/100)%10))+((x/10)%10 +x%10))%4==0).toArray(Integer[]::new);
        Comparator<Integer> by = Comparator.comparing(x->x);
        System.out.println(treeSort.sort(by.reversed(),data));
    }

    public static void task10(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Integer[] data = {456765,469465,469321,616321,576567,469563,567564,469129,657665,469873,569090,469999,564321,469010};
        data = Arrays.stream(data).filter(x->(x/1000)==469).toArray(Integer[]::new);
        Comparator<Integer> by = Comparator.comparing(x->x);
        System.out.println(treeSort.sort(by.reversed(),data));
    }

    public static void task11(){
        TreeSort<Student> treeSort = new TreeSort<>();
        Student[] data = new Student[10];
        Random rd = new Random();
        for (int i = 0; i < 10; i++) {
            data[i] = new Student(
                    String.valueOf(i),
                    String.valueOf(i),
                    i,
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100),
                    rd.nextInt(0,100)
            );
        }
        treeSort.sort(Student.byTotalScore.reversed(), data).forEach(x->System.out.print(x+", "));
        System.out.println();
    }
    public static void task12(){
        TreeSort<Integer> treeSort = new TreeSort<>();
        Random rd = new Random();
        Integer[] data = new Integer[100];
        for (int i = 0; i < 100; i++) {
            data[i] = rd.nextInt(1000,9999);
        }
        data = Arrays.stream(data).filter(x->((((x/1000)%10)+((x/100)%10))+(x/10)%10 )==8).toArray(Integer[]::new);
        if (data.length<=1){System.out.println(Arrays.toString(data)); return;}
        Comparator<Integer> by = Comparator.comparing(x->x);
        System.out.println(treeSort.sort(by.reversed(),data));
    }




    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
    }
}
