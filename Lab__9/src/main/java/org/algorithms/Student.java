package org.algorithms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
//@Builder
public class Student {

    private String surname;
    private String name;
    private Integer wasBorn;
    private Integer mathScore;
    private Integer englishScore;
    private Integer physicsScore;
    private Integer programmingScore;
    private Integer biologyScore;
    private Integer totalScore;

    public Student(String surname, String name, Integer year, Integer mathScore, Integer englishScore, Integer physicsScore, Integer programmingScore, Integer biologyScore) {
        this.surname = surname;
        this.name = name;
        this.wasBorn = year;
        this.mathScore = mathScore;
        this.englishScore = englishScore;
        this.physicsScore = physicsScore;
        this.programmingScore = programmingScore;
        this.biologyScore = biologyScore;
        this.totalScore = mathScore+englishScore+physicsScore+programmingScore+biologyScore;
    }


    @Override
    public String toString() {
        return "Student{" +
                "totalScore=" + totalScore +
                '}';
    }

    public static Comparator<Student> byName= Comparator.comparing(Student::getName);
    public static Comparator<Student> bySurname = Comparator.comparing(Student::getSurname);
    public static Comparator<Student> byWasBorn = Comparator.comparing(Student::getWasBorn);
    public static Comparator<Student> byMathScore = Comparator.comparing(Student::getMathScore);
    public static Comparator<Student> byEnglishScore = Comparator.comparing(Student::getEnglishScore);
    public static Comparator<Student> byPhysicsScore = Comparator.comparing(Student::getPhysicsScore);
    public static Comparator<Student> byProgrammingScore = Comparator.comparing(Student::getProgrammingScore);
    public static Comparator<Student> byTotalScore = Comparator.comparing(Student::getTotalScore);

}