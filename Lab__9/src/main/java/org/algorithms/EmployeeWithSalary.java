package org.algorithms;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeWithSalary {
    String secondName;
    Double salary;
}
