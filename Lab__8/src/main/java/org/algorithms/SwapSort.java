package org.algorithms;

import java.util.*;


public class SwapSort {

    public static  <T> ArrayList<T> quickSort(ArrayList<T> data, Comparator<T> by){
        if(data.size() < 2){
            return data;
        }else{
            ArrayList<T> left = new ArrayList<>();
            ArrayList<T> right = new ArrayList<>();
            T middle = data.get(0);
            for (int i=1; i<data.size(); i++) {
                if(by.compare(middle, data.get(i))>0){
                    left.add(data.get(i));
                }else {
                    right.add(data.get(i));
                }
            }
            left = quickSort(left, by);
            right = quickSort(right, by);

            left.add(middle);
            left.addAll(right);
            return left;
        }
    }

    public static <T> void shakerSort(ArrayList<T> data, Comparator<T> by) {
        int left, right, i, n=data.size();
        left = 0;
        right= n - 1;
        while (left <= right) {
            for (i = right; i > left; i--) {
                if (by.compare(data.get(i-1) ,data.get(i))>0) {
                    Collections.swap(data, i-1, i);
                }
            }
            left++;
            for (i = left; i <= right; i++) {
                if (by.compare(data.get(i-1) ,data.get(i))>0) {
                    Collections.swap(data, i-1, i);
                }
            }
            right--;
        }
    }


    public static<T> T minValue(ArrayList<T> data, Comparator<T> by){
        T min = data.get(0);
        for (T datum : data) {
            if (by.compare(datum , min) < 0) {
                min = datum;
            }
        }
        return min;
    }

    public static <T> T task1(ArrayList<T> data, Comparator<T> by, boolean max){
        if(max)  {shakerSort(data, by.reversed());}
        else shakerSort(data, by);
        return data.get(0);
    }

    public static  <T> void task2(ArrayList<T> data, Comparator<T> by){
        shakerSort(data,by.reversed());
    }

    public static  <T> ArrayList<T> task3(ArrayList<T> data, Comparator<T> by){
        ArrayList<T> b = new ArrayList<>(data);
        shakerSort(b,by.reversed());
        return b;
    }

    public  static <T> void task4(ArrayList<T> data, Comparator<T> by, T item){
        for (int i = 0; i < data.size(); i++) {
            if (by.compare(data.get(i), item) < 0) continue;
            data.add(i, item);
            return;
        }
        data.add(item);
    }

    public  static <T> void task5(ArrayList<T> data){
        int i = data.size()/2;
        int p1 = 0;
        int p2 = data.size()-1;

        while (p1<i){
            Collections.swap(data, p1, p2);
            p1++;
            p2--;
        }
    }

    public static void task6(ArrayList<Double> data){
        ArrayList<Double> filtered = new ArrayList<>();
        for (Double x:data) {
            if(x >= 0D)filtered.add(x);
        }
        data.clear();
        data.addAll(filtered);
        shakerSort(data, Double::compareTo);
    }

    public static <T> ArrayList<T> task7(ArrayList<T> A, Comparator<T> by){
        ArrayList<T> B = new ArrayList<>();
        T tmp;
        final int size = A.size();

        for (int i =0; i<size; i++) {
            tmp = minValue(A, by);
            B.add(tmp);
            A.remove(tmp);
        }
        return B;
    }


    public static void task8(ArrayList<String> data){
        Comparator<String> by = Comparator.comparing(String::toString);
         shakerSort(data, by);
    }

    public static void task9(ArrayList<TelephoneStationSubscriber> data){
        Comparator<TelephoneStationSubscriber> by = Comparator.comparing(TelephoneStationSubscriber::getPhoneNumber);
        shakerSort(data, by);
    }

    public static void task10(ArrayList<String> data){
        Comparator<String> by = Comparator.comparing(String::length);
        shakerSort(data, by);
    }


    public static void task11(ArrayList<Double> data, boolean desc){
        Comparator<Double> by = Comparator.comparing(Math::abs);
        if (desc){
        shakerSort(data, by.reversed());
        }else shakerSort(data, by);
    }

    public static void task12(ArrayList<Double> data){
        int size = data.size();
        for (int i = 0; i < size-1; i++) {
            Double a = data.get(i);
            Double b = data.get(i+1);
         if(!a.equals(b)){
             data.add(i+1, (a+b)/2D);
             i+=1;
             size+=1;
         }
        }
    }

    public static <T> void outputBefore(int task, ArrayList<T> before){
        System.out.println("_____________________________________________________________\nTask "+ task+"\n______");
        System.out.print("До:    ");
        before.forEach(x->System.out.print(x+", "));
        System.out.println();
    }

    public static <T> void outputAfter(ArrayList<T> after){
        System.out.print("После: ");
        after.forEach(x->System.out.print(x+", "));
        System.out.println("\n");
    }




    public static void main(String[] args) {

        ArrayList<Double> arr = new ArrayList<>(List.of(-4D, -3D, -2D,-1D, 0D, 1D, 2D, 3D, 4D));

        ArrayList<Double> x = new ArrayList<>(arr);
        Comparator<Double> by = Comparator.comparingDouble(y->y);


        outputBefore(1, x);
        Double z1 = task1(x, by, true);
        System.out.println("Max: "+z1);
        Double z2 = task1(x, by, false);
        System.out.println("Min: "+z2);
        System.out.println();

        x = new ArrayList<>(arr);
        outputBefore(2, x);
        task2(x, by);
        outputAfter(x);

        x = new ArrayList<>(arr);
        outputBefore(3,x);
        ArrayList<Double> x1 = task3(x, by);
        outputAfter(x1);


        x = new ArrayList<>(arr);
        outputBefore(4,x);
        task4(x, by, 17D);
        outputAfter(x);


        x =new ArrayList<>(arr);
        outputBefore(5,x);
        task5(x);
        outputAfter(x);

        x = new ArrayList<>(arr);
        outputBefore(6,x);
        task6(x);
        outputAfter(x);


        x = new ArrayList<>(List.of(4D, 3D, -5D,-10D, 20D, 11D, 2D, 13D, 1D));
        outputBefore(7,x);
        ArrayList<Double> x2 = task7(x, Comparator.comparing(v->v));
        outputAfter(x2);


        ArrayList<String> y = new ArrayList<>(List.of("Арбузов", "Яблоков", "Буйволов", "Васильков"));
        outputBefore(8, y);
        task8(y);
        outputAfter(y);


        ArrayList<TelephoneStationSubscriber> q = new ArrayList<>(List.of(new TelephoneStationSubscriber("1111", "Арбузов"),
                                                                          new TelephoneStationSubscriber("4444", "Яблоков"),
                                                                          new TelephoneStationSubscriber("2222", "Буйволов"),
                                                                          new TelephoneStationSubscriber("3333", "Васильков") ));
        outputBefore(9, q);
        task9(q);
        outputAfter(q);


        ArrayList<String> w = new ArrayList<>(List.of("Арбузов", "Яблок", "Буйволова", "Василькович"));
        outputBefore(10,w);
        task10(w);
        outputAfter(w);


        x = new ArrayList<>(arr);
        outputBefore(11, x);
        System.out.println();
        task11(x,false);
        System.out.println("ASC");
        outputAfter(x);
        task11(x,true);
        System.out.println("DESC");
        outputAfter(x);


        x = new ArrayList<>(arr);
        outputBefore(12, x);
        task12(x);
        outputAfter(x);

    }
}
