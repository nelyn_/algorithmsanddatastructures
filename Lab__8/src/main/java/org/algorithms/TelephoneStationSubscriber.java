package org.algorithms;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class TelephoneStationSubscriber {
    private final String phoneNumber;
    private final String SecondName;

    public String toString(){
        return phoneNumber + " : " + SecondName;
    }
}
