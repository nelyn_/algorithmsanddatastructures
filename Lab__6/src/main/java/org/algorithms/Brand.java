package org.algorithms;

public enum Brand {
    Mercedes("Мерседес"),
    Tesla("Тесла"),
    BMW("БМВ"),
    Ferrari("Феррари"),
    Ford("Форд"),
    Porsche("Порше"),
    Honda("Хонда"),
    Lamborghini("Ламборгини"),
    Zhiguli("Жигули");

    public final String value;

    private Brand(String value) {
        this.value = value;
    }

}
