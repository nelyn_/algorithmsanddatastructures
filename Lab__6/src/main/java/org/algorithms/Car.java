package org.algorithms;

import java.util.Comparator;
import java.util.Date;

public class Car {

    private Integer number;
    private Brand brand;
    private String owner;
    private Date lastFix;
    private Date shouldBeFinishedTo;

    public Car(Integer number, Brand brand, String owner, Date lastFix, Date shotBeFinishedTo) {
        this.number = number;
        this.brand = brand;
        this.owner = owner;
        this.lastFix = lastFix;
        this.shouldBeFinishedTo = shotBeFinishedTo;
    }

    @Override
    public String toString() {
        return String.format("NUM %5s(%15s\t%10s\t%10tD\t%10tD)", getNumber(), getBrand().value, getOwner(), getLastFix(), getShouldBeFinishedTo());
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getLastFix() {
        return lastFix;
    }

    public void setLastFix(Date lastFix) {
        this.lastFix = lastFix;
    }

    public Date getShouldBeFinishedTo() {
        return shouldBeFinishedTo;
    }

    public void setShotBeFinishedTo(Date shotBeFinishedTo) {
        this.shouldBeFinishedTo = shotBeFinishedTo;
    }

    public static Comparator<Car> byNumber = Comparator.comparing(Car::getNumber);
    public static Comparator<Car> byOwner = Comparator.comparing(Car::getOwner);
    public static Comparator<Car> byBrand = Comparator.comparing(v->v.getBrand().value);
    public static Comparator<Car> byLastFix = Comparator.comparing(Car::getLastFix);
    public static Comparator<Car> byShouldBeFinishedTo = Comparator.comparing(Car::getShouldBeFinishedTo);


}
