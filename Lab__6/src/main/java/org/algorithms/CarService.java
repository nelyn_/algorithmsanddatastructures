package org.algorithms;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class CarService {

    public static <T>void inclusionSort(List<T> a, Comparator<T> by){
        for (int j = 0; j < a.size(); j++) {
            T current = a.get(j);
            int i = j-1;
            while(i >= 0 && by.compare(current,a.get(i))<0){
                a.set(i+1, a.get(i));
                i--;
            }
        a.set(i+1, current);
       }
    }

    public static List<Car> filter(List<Car> cars, Predicate<Car> filter){
        return cars.stream().filter(filter).collect(Collectors.toList());
    }

    //Variant 1
    public static void task1(List<Car> cars){
        inclusionSort(cars, Car.byOwner);
    }

    //Variant 2
    public static void task2(List<Car> cars){
        inclusionSort(cars, Car.byShouldBeFinishedTo);
    }

    //Variant 3
    public static List<Map.Entry<Integer, Long>> task3(List<Car> cars, boolean distinct){
        cars =  filter(cars, v->v.getBrand().equals(Brand.Zhiguli));
        List<Map.Entry<Integer, Long>> y = new ArrayList<>(cars.stream().collect(Collectors.groupingBy(Car::getNumber, Collectors.counting())).entrySet());
        if(distinct){
            inclusionSort(y, Map.Entry.comparingByValue(Comparator.reverseOrder()));
        } else {
            inclusionSort(y, Map.Entry.comparingByValue());
        }
        return y;
    }

    //Variant 4
    public static List<Map.Entry<Integer, Long>> task4(List<Car> cars, boolean distinct){
        List<Map.Entry<Integer, Long>> y = cars.stream().collect(Collectors.groupingBy(Car::getNumber, Collectors.counting())).entrySet().stream().filter(v->v.getValue()==2).collect(Collectors.toList());
        if(distinct){
            inclusionSort(y, Map.Entry.comparingByKey(Comparator.reverseOrder()));
        } else {
            inclusionSort(y, Map.Entry.comparingByKey());
        }
        return y;
    }

    //Variant 5
    public static List<Car> task5(List<Car> cars){
        List<Car> cars2 = filter(cars, (x)->x.getLastFix()!=null);
        inclusionSort(cars2, Car.byShouldBeFinishedTo);
        return cars2;
    }

    //Variant 6
    public static List<Car> task6(List<Car> cars){
        List<Car> cars2 = filter(cars, (x)->x.getBrand().equals(Brand.Mercedes));
        inclusionSort(cars2, Car.byOwner.reversed());
        return cars2;
    }
    //Variant 7
    public static List<Car> task7(List<Car> cars) throws ParseException{
        Date d = new SimpleDateFormat("dd/MM/yyyy").parse("01/08/1996");
        List<Car> cars2 = filter(cars, (x)-> x.getShouldBeFinishedTo().compareTo(d)<0);
        inclusionSort(cars2, Car.byBrand);
        return cars2;
    }

    //Variant 8
    public static List<Car> task8(List<Car> cars){
        List<Car> cars2 = filter(cars, (x)->x.getBrand().equals(Brand.Zhiguli));
        inclusionSort(cars2, Car.byNumber);
        return cars2;
    }

    //Variant 9
    public static List<Car> task9(List<Car> cars) throws ParseException{
        Date d = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2020");
        List<Car> cars2 = filter(cars, (x)-> x.getLastFix().compareTo(d)<0);
        inclusionSort(cars2, Car.byOwner);
        return cars2;
    }

    //Variant 10
    public static List<Car> task10(List<Car> cars) throws ParseException{
        Date d = new SimpleDateFormat("dd/MM/yyyy").parse("01/10/2020");
        List<Car> cars2 = filter(cars, (x)-> x.getShouldBeFinishedTo().compareTo(d)<0);
        inclusionSort(cars2, Car.byLastFix);
        return cars2;
    }

    //Variant 11
    public static List<Map.Entry<AbstractMap.SimpleEntry<Integer,String>, Long>> task11(List<Car> cars, boolean distinct){
        List<Map.Entry<AbstractMap.SimpleEntry<Integer,String>, Long>> y = cars.stream().collect(Collectors.groupingBy(x-> new AbstractMap.SimpleEntry<>(x.getNumber(), x.getOwner()) , Collectors.counting())).entrySet().stream().filter(v->v.getValue()>3).collect(Collectors.toList());
        if(distinct){
            inclusionSort(y, Comparator.comparing(x->x.getKey().getValue(), Comparator.reverseOrder()));
        } else {
            inclusionSort(y, Comparator.comparing(x->x.getKey().getValue()));
        }
        return y;
    }

    //Variant 12
    public static List<Car> task12(List<Car> cars){
        List<Car> cars2 = filter(cars, (x)->x.getBrand().equals(Brand.Mercedes));
        inclusionSort(cars2, Car.byNumber.reversed());
        return cars2;
    }



    public static void main(String[] args) throws ParseException {

        List<Car> cars = new LinkedList<>();
        SimpleDateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

        cars.add(new Car(1111,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(1111,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(1111,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(1111,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(1212,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(1212,Brand.Zhiguli,    "Алексей", dF.parse( "04/04/2014"), dF.parse("07/07/2017")));
        cars.add(new Car(2222,Brand.Lamborghini,"Григорий",dF.parse( "03/03/2013"), dF.parse("06/06/2016")));
        cars.add(new Car(3333,Brand.Mercedes,   "Зинаида", dF.parse( "02/02/2012"), dF.parse("05/05/2015")));
        cars.add(new Car(3333,Brand.Mercedes,   "Зинаида", dF.parse( "02/02/2012"), dF.parse("05/05/2015")));
        cars.add(new Car(4444,Brand.BMW,        "Борис",   dF.parse( "01/01/2010"), dF.parse("08/08/2018")));
        cars.add(new Car(5555,Brand.Ferrari,    "Иван",    dF.parse( "08/08/2021"), dF.parse("09/09/2019")));
        cars.add(new Car(6666,Brand.Ford,       "Карина",  dF.parse( "09/09/2020"), dF.parse("01/01/2011")));
        cars.add(new Car(7777,Brand.Honda,      "Жанна",   dF.parse( "07/07/2017"), dF.parse("12/12/2021")));
        cars.add(new Car(8888,Brand.Porsche,    "Евгений", dF.parse( "06/06/2016"), dF.parse("10/10/2020")));
        cars.add(new Car(8888,Brand.Porsche,    "Евгений", dF.parse( "06/06/2016"), dF.parse("10/10/2020")));
        cars.add(new Car(8888,Brand.Porsche,    "Евгений", dF.parse( "06/06/2016"), dF.parse("10/10/2020")));
        cars.add(new Car(8888,Brand.Porsche,    "Евгений", dF.parse( "06/06/2016"), dF.parse("01/05/1996")));
        cars.add(new Car(9999,Brand.Tesla,      "Владимир",dF.parse( "05/05/2015"), dF.parse("01/06/1996")));
        cars.add(new Car(1010,Brand.Mercedes,   "Владимир",dF.parse( "05/05/2021"), dF.parse("01/07/1996")));


        /* 1 по владельцу */
        task1(cars);
        cars.forEach(System.out::println);
        System.out.println();

        /* 2 по дате готовности */
        task2(cars);
        cars.forEach(System.out::println);
        System.out.println();

        /* 3 по last fixes*/
        task3(cars, true).forEach(x-> System.out.printf("Номер  %4d, Количество предыдущих ремонтов %3d%n", x.getKey(), x.getValue()));
        System.out.println();

        /* 4 по 2 fixes */
        task4(cars, true).forEach(x-> System.out.printf("Номер  %4d, Количество предыдущих ремонтов %3d%n", x.getKey(), x.getValue()));
        System.out.println();

        /* 5  */
        task5(cars).forEach(System.out::println);
        System.out.println();

        /* 6 */
        task6(cars).forEach(System.out::println);
        System.out.println();

        /* 7 */
        task7(cars).forEach(System.out::println);
        System.out.println();

        /* 8 */
        task8(cars).forEach(System.out::println);
        System.out.println();

        /* 9 */
        task9(cars).forEach(System.out::println);
        System.out.println();

        /* 10 */
        task10(cars).forEach(System.out::println);
        System.out.println();

        /* 11 */
        task11(cars, true).forEach(x->System.out.printf("Номер  %4d, Владелец: %8s, Количество предыдущих ремонтов: %3d%n", x.getKey().getKey(),x.getKey().getValue(), x.getValue()));
        System.out.println();

        /* 12 */
        task12(cars).forEach(System.out::println);
        System.out.println();


    }

}
